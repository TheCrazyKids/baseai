# Alice Commands
Alice can be accessed using the `/baseai` chat command. All commands are prefixed with `System Call,` followed by the command.

Commands containing a {} specify an argument, valid arguments are shown in the proceeding documentation.
___
## Update AI Database
**Purpose**: Get the BaseAI to pull the latest data from the repo.

**Users**: Superadmin Only
> Documentation Command, not here for common use.
___
## `{1}` Main Base Gate
**Purpose**: Open, close or toggle the main base gate.

**Users**: Clearance Level 3 or user in vehicle.

**Aliases**: `{1}` Main Gate

`{1}`: Open, Close, Toggle
___
## `{1}` Side Hanger Bay
**Purpose**: Open, close or toggle the side hanger door.

**Users**: Clearance Level 3 or user in vehicle.

**Aliases**: `{1}` Side Hanger

`{1}`: Open, Close, Toggle
___
## Toggle Brig Rayshield
**Purpose**: Toggle the brig rayshield

**Users**: Clearance Level 5, assigned CG Jedi, Template Guard or Clone Guard.
___
## Toggle Brig Lockdown
**Purpose**: Toggle the brig lockdown

**Users**: Clearance Level 5, assigned CG Jedi or Clone Guard.

**Aliases**: Lockdown Brig, Brig Lockdown
___
## Open The Panic Room 9714
**Purpose**: Toggle the panic room.

**Users**: All.

**Aliases**: Toggle Panic Room 9714, Toggle Panic 9714, Panic 9714
> This command requires re-evaluation and will likely be changed at a later date.
___
## Open The Panic Room
**Purpose**: Tell the user to append the correct code

**Users**: All.

**Aliases**: Toggle Panic Room, Toggle Panic, Panic
> This command is deprecated and will likely be removed at a later date.
___